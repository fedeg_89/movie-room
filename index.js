const fs = require("fs")
const http = require("http")
// const url = require("url")
const path = require("path")

http.createServer(function (req, res) {
	if (req.url != "/video") {
		res.writeHead(200, { "Content-Type": "text/html" })
		res.end('<video src="http://localhost:3000/video" controls></video>')
	} else {
		const file = path.resolve(__dirname,"assets/sample.mp4")
		fs.stat(file, function(err, stats) {
			if (err) {
				if (err.code === 'ENOENT') {
					console.log(err)
					// 404 Error if file not found
					return res.sendStatus(404)
				}
			res.end(err)
			}
			const range = req.headers.range
			if (!range) {
				// 416 Wrong range
				return res.sendStatus(416)
			}
			const positions = range.replace(/bytes=/, "").split("-")
			const start = parseInt(positions[0], 10)
			const total = stats.size
			const end = positions[1] ? parseInt(positions[1], 10) : total - 1
			const chunksize = (end - start) + 1
			res.writeHead(206, {
				"Content-Range": "bytes " + start + "-" + end + "/" + total,
				"Accept-Ranges": "bytes",
				"Content-Length": chunksize,
				"Content-Type": "video/mp4"
			})

			const stream = fs.createReadStream(file, { start: start, end: end })
				.on("open", function() {
					stream.pipe(res)
				})
				.on("error", function(err) {
					res.end(err)
				})
		})
  }
}).listen(3000)

const Message = require('./classes/Message')
const CustomWWS = require('./classes/CustomWWS')
const sendUsersNames = require('./helpers/sendUsersNames')
 
const wss = new CustomWWS({ port: 8080 })
 
let counterId = 1

wss.on('connection', function connection(ws) {
	ws.id = counterId ++

  ws.on('message', function incoming(wsData) {
		console.log(wsData)

		const comand = wsData.split(': ')[0]
		const data = wsData.split(': ')[1]

		switch (comand) {
			case 'name':
				console.log(data)
				// Assign name to the socket
				if (data == 'true' || data == '') { // anonymus || blank
					ws.name = `Guest ${ws.id}`
				} else {
					ws.name = data
				}

				// Sends the socket the name confirmation
				msg = new Message('name', ws.name)
				ws.send(JSON.stringify(msg))

				// Sends to all users the refreshed list of users conected
				sendUsersNames(ws.name, 'conected', wss)
				break
		
			case 'chatMsg':
				// sends everybody the message
				msg = new Message('chatMsg', data, ws.name)
				wss.sendEverybody(msg)
				break

			case 'typing':
				msg = new Message('typing', data, 'server')
				wss.sendEverybody(msg)
				break

			case 'video':
					msg = new Message('video', data, ws.name)
					wss.sendEverybody(msg)
				break

			default:
					let text = `Unknwon comand '${comand}'`
					msg = new Message('unknown', text)
					ws.send(JSON.stringify(msg))
				break
		}
	})
	
	ws.on('close', function close() {
		console.log(`${ws.id}: ${ws.name} disconected`)
		
		sendUsersNames(ws.name, 'disconected', wss)
	})
})



