const WebSocket = require('../node_modules/ws/lib/websocket');
const WebSocketServer = require('../node_modules/ws/lib/websocket-server');

class CustomWWS extends WebSocketServer {
	constructor(options, callback) {
		super(options, callback)

		this.OPEN = WebSocket.OPEN
		this.CONNECTING = WebSocket.CONNECTING
		this.CLOSING = WebSocket.CLOSING
		this.CLOSED = WebSocket.CLOSED
	}

	sendEverybody(msg, ws = undefined) { // If ws is passed, exclude it from broadcasting
		this.clients.forEach(client => {
			if (client !== ws && client.readyState === this.OPEN) {
				client.send(JSON.stringify(msg))
			}
		})
	}

	getClients() {
		let usersNames = []
		this.clients.forEach(client => usersNames.push(client.name))
		return usersNames
	}
}

module.exports = CustomWWS