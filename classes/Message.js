class Message {
	constructor(comand, data, name) {
		this.comand = comand
		this.data = data
		this.name = name
		this.date = this.getTime()
	}

	getTime() {
		const date = new Date()
		const hour = `${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`
		return hour
	}

	getClients() {
		const usersNames = []
		this.clients.forEach(client => usersNames.push(client.name))
		return usersNames
	}
}

module.exports = Message