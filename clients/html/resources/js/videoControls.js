const videoPlayer = document.getElementById("videoPlayer")
const usersList = document.getElementById("usersList")
const chat = document.getElementById("chat")
const chatText = document.getElementById("chatText")
const inputName = document.getElementById("inputName")
const socket = new WebSocket('ws://localhost:8080')
let userName = ''

videoPlayer.addEventListener("seeking", e => {
	console.log('seeking: ', videoPlayer.currentTime) // videoPlayer.currentTime
	// socket.send(`video: ${videoPlayer.currentTime}`)
})

videoPlayer.addEventListener("seeked", e => {
	console.log('seeked: ', videoPlayer.currentTime) // videoPlayer.currentTime
})

videoPlayer.addEventListener("play", e => {
	console.log('play: ', videoPlayer.currentTime) // videoPlayer.currentTime
	socket.send('video: play')
})

videoPlayer.addEventListener("pause", e => {
	console.log('pause: ', videoPlayer.currentTime) // videoPlayer.currentTime
	socket.send('video: pause')
})

// Connection opened
socket.addEventListener('open', function (event) {
	socket.send('Hello Server!')
})

// Listen for messages
socket.addEventListener('message', function (event) {
	console.log('Message from server ', event.data);

	const { comand, data, name, date } = JSON.parse(event.data)

	switch (comand) {
		case 'name':
			userName = data
			document.getElementById('tvesModal').style.display='none'
			break

		case 'users':
			usersList.innerHTML = ''
			data.forEach(user => {
				usersList.innerHTML += `<li>${user}</li>`
			})
			break
	
		case 'chatMsg':
			chat.innerHTML += `<span class="${getChatClass(name)}">${date} ${returnName(name)} ${data}</span>`
			break

		case 'video':
			if (typeof data == 'string') {
				videoPlayer[data].call(videoPlayer)
				chat.innerHTML += `<span class="server">${date} ${(name)} ${data}d the video.</span>`
			} else {
				videoPlayer.currentTime = data
				chat.innerHTML += `<span class="server">${date} ${(name)} changed video time to ${data}.</span>`
			}
			break
		default:
		break
	}
})

function getChatClass(name) {
	switch (name) {
		case userName:
			return 'self'
			break;
		case 'server':
			return 'server'
			break;
		default:
			return 'other'
	}
}

function returnName(name) {
	switch (name) {
		case 'server':
			return ''
			break;
		case userName:
			return 'you:'
			break;
		default:
			return `${name}:`
	}
}

function sendMsg() {
	socket.send(`chatMsg: ${chatText.value}`)
	chatText.value = ''
}

chatText.addEventListener('keyup', e => {
	if (e.keyCode === 13) sendMsg()
})

function sendName(anonymus = false) {
	socket.send(`name: ${anonymus || inputName.value}`)
	inputName.value = ''
}