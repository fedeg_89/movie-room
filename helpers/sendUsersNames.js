const Message = require('../classes/Message')

const sendUsersNames = (name, event, wss) => {
	// Get the list of users conected
	const usersNames = wss.getClients()
	// The message with the list
	let msg1 = new Message('users', usersNames)
	// The message to the chat to infrom conection/desconection
	const text = `${name} ${event}`
	let msg2 = new Message('chatMsg', text, 'server')

	wss.sendEverybody(msg1)
	wss.sendEverybody(msg2)
}

module.exports = sendUsersNames